function exibirTorre() {
    const torreDiv = document.getElementById('gameContent')

    const torreDiv1 = document.createElement('div')


    for (let i = 1; i < 5; i++) {
        const bloco = document.createElement('div')
        bloco.classList.add('bloco')
        bloco.id = `bloco${i}`
        torreDiv1.appendChild(bloco)
    }


    const torreDiv2 = document.createElement('div')
    const torreDiv3 = document.createElement('div')


    torreDiv1.classList.add('torre__bloco')
    torreDiv2.classList.add('torre__bloco')
    torreDiv3.classList.add('torre__bloco')


    const torre = document.createElement('div')
    const torre2 = document.createElement('div')
    const torre3 = document.createElement('div')



    torre.classList.add('torre')
    torre2.classList.add('torre')
    torre3.classList.add('torre')

    torreDiv1.appendChild(torre)
    torreDiv2.appendChild(torre2)
    torreDiv3.appendChild(torre3)

    torreDiv.appendChild(torreDiv1)
    torreDiv.appendChild(torreDiv2)
    torreDiv.appendChild(torreDiv3)


}

function moverBloco() {
    let selecionado = false
    let blocoSelect = ''

    const selectBloco = (e) => {
        e.preventDefault()
        const torre = e.currentTarget

        const bloco = torre.querySelector('.bloco')

        if (bloco === null) {
            alert('Essa Torre não possui nenhum bloco.')
            return
        }

        blocoSelect = bloco
        selecionado = true
    }

    const moverBloco = (e) => {
        if (blocoSelect) {
            const torrePai = blocoSelect.parentNode

            const torreMove = e.currentTarget
            torreMove.insertAdjacentElement('afterbegin', blocoSelect)

            const blocoDeBaixo = torreMove.querySelectorAll('.bloco')[1]

            if (blocoDeBaixo !== undefined) {
                if (blocoDeBaixo.offsetWidth < blocoSelect.offsetWidth) {
                    alert('Não é permitido que bloco embaixo seja menor')
                    torrePai.insertAdjacentElement('afterbegin', blocoSelect)
                }
            }

            const qtdBlocosTorre3 = document.querySelectorAll('.torre__bloco')[2]
                .querySelectorAll('.bloco').length

            if (qtdBlocosTorre3 === 4) {
                let resultBox = document.getElementById('result');
                let div = document.createElement('div');
                div.id = 'ganhou';
                resultBox.appendChild(div);
                div.textContent = "Você Ganhou !!";
                // alert('Parabéns você ganhou!')
            }
            console.log(qtdBlocosTorre3);

            blocoSelect = ''
            selecionado = false
        }


    }

    document.querySelectorAll('.torre__bloco')
        .forEach(torre => {
            torre.addEventListener('click', (e) => {
                if (!selecionado) {
                    selectBloco(e)
                } else {
                    moverBloco(e)
                }
            })
        })




}

exibirTorre()
moverBloco()